﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenSystemLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using TowerDefenceEngine;

namespace TowerDefenceGame
{
    public class PlayScreen : GameScreen
    {
        UserInterface ui;
        Map map;
        Session session;
        List<Tower> towers;
        SpriteFont font;
        LevelSelectionScreen levelSelect;

        public override bool AcceptsInput
        {
            get { return true; }
        }

        public PlayScreen(LevelSelectionScreen lss, Map m)
        {
            map = m;
            levelSelect = lss;

            towers = new List<Tower>(10);
            session = new Session(m);

            session.HealthDecreased += new EventHandler(session_HealthDecreased);
            session.MapFinished += new EventHandler(session_MapFinished);
        }

        void session_MapFinished(object sender, EventArgs e)
        {
            ExitScreen();

            if (session.Health >= 0)
            {
                levelSelect.ActivateScreen();
            }

            else
            {
                ScreenSystem.AddScreen(new GameOverScreen(levelSelect));
            }
        }

        void session_HealthDecreased(object sender, EventArgs e)
        {
            if (session.Health < 0)
            {
                ExitScreen();
                ScreenSystem.AddScreen(new GameOverScreen(levelSelect));
            }
        }

        public override void InitializeScreen()
        {
            // Change for dynamic resolutions
            ui = new UserInterface(
                new MapRegion(session, new Rectangle(0, 0, 960, 690), ScreenSystem.GraphicsDevice),
                new WaveInformation(session, new Rectangle(0, 690, 1280, 30), ScreenSystem.GraphicsDevice, Color.Black),
                new CommandInfoBar(session, new Rectangle(960, 0, 320, 720), ScreenSystem.GraphicsDevice),
                session);

            AudioManager.singleton.PlaySong(map.SongCueName);
            InputMap.NewAction("Pause", Microsoft.Xna.Framework.Input.Keys.Escape);
        }

        public override void LoadContent()
        {
            ui.Font = ScreenSystem.Content.Load<SpriteFont>("Fonts\\playfont");
        }

        protected override void UpdateScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (session.IsPaused)
            {
                FreezeScreen();
                ScreenSystem.AddScreen(new PauseScreen(this, session));
            }

            if (InputMap.NewActionPress("Pause"))
            {
                session.Pause();
            }

            session.Update(gameTime);
        }

        protected override void DrawScreen(Microsoft.Xna.Framework.GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenSystem.SpriteBatch;

            ui.DrawUI(gameTime, spriteBatch);
        }
    }
}
