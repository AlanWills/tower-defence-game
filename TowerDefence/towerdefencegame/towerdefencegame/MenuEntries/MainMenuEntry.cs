﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScreenSystemLibrary;

namespace TowerDefenceGame
{
    public class MainMenuEntry : MenuEntry
    {
        public MainMenuEntry(MenuScreen menu, string title, string description)
            : base(menu, title)
        {
            EntryDescription = description;
        }

        public override void AnimateHighlighted(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Animate when an entry is highlighted like pulsate.
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Update all entries
        }
    }
}
