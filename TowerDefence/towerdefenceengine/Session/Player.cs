﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TowerDefenceEngine
{
    public class Player
    {
        #region Properties and Fields

        public uint Money
        {
            get;
            set;
        }

        public List<Tower> PlacedTowers
        {
            get;
            set;
        }

        #endregion
    }
}
