﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TowerDefenceEngine
{
    public class Monster : GameplayObject
    {
        #region Writer

        [ContentTypeWriter]
        public class MonsterWriter : ContentTypeWriter<Monster>
        {
            GameplayObjectWriter gameplayObjectWriter = null;

            protected override void Initialize(ContentCompiler compiler)
            {
                gameplayObjectWriter = compiler.GetTypeWriter(typeof(GameplayObject)) as GameplayObjectWriter;
                
                base.Initialize(compiler);
            }

            protected override void Write(ContentWriter output, Monster value)
            {
                output.WriteRawObject<GameplayObject>(value as GameplayObject, gameplayObjectWriter);
                output.Write(value.Health);
                output.Write(value.TextureAsset);
                output.Write(value.HitTextureAsset);
                output.Write(value.HitCueName);
                output.Write(value.IsBoss);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(Monster.MonsterReader).AssemblyQualifiedName;
            }
        }


        #endregion

        #region Properties

        [ContentSerializer]
        public int Health
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public int MaxHealth
        {
            get;
            private set;
        }

        [ContentSerializer]
        private string TextureAsset
        {
            get;
            set;
        }

        [ContentSerializer]
        private string HitTextureAsset
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public Texture2D HitTexture
        {
            get;
            private set;
        }

        [ContentSerializer]
        public string HitCueName
        {
            get;
            private set;
        }

        [ContentSerializer(Optional = true)]
        public bool IsBoss
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public Wave Wave
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public int PathIndex
        {
            get;
            protected set;
        }

        [ContentSerializerIgnore]
        public float DistanceToTravel
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public float DistanceTravelled
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public float Delay
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public bool IsActive
        {
            get { return Delay <= 0 && Status == ObjectStatus.Active; }
        }

        #endregion

        #region Fields

        float hitTimer;
        float statisticsTimer;

        Text hitDisplay;

        float deltaHealth;
        float DoTTimer;
        float DoTTime;
        int dotHealth;

        public event EventHandler DieEvent;
        public event EventHandler HitEvent;

        #endregion

        #region Constructor

        public Monster()
        {
            DistanceToTravel = float.MinValue;
            DistanceTravelled = float.MaxValue;
        }

        #endregion

        #region Methods

        public override void Update(GameTime gameTime)
        {
            if (dotHealth > 0 && DoTTime > 0)
            {
                UpdateDoT(gameTime);

                if (DoTTimer > DoTTime)
                {
                    deltaHealth = 0.0f;
                    dotHealth = 0;
                    DoTTime = 0;
                    DoTTimer = 0;
                }
            }

            if (hitTimer > 0)
            {
                hitTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed;
                hitDisplay.Update(gameTime);

                if (hitTimer <= 0)
                {
                    hitDisplay = null;
                }
            }

            if (Delay > 0)
            {
                Delay -= (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed;
            }

            else
            {
                Pathfinding ShortestPath = Wave.Path.ShortestPath;

                if (PathIndex == ShortestPath.path.Count && DistanceTravelled > DistanceToTravel)
                {
                    Die();
                    Wave.Remove(this);
                }
                else
                {
                    int i = PathIndex + 1;
                    if (DistanceTravelled > DistanceToTravel)
                    {
                        if (i < ShortestPath.path.Count)
                        {
                            NewNodeInPath(ShortestPath.path[i]);
                        }
                        else
                        {
                            NewNodeInPath(Wave.Path.End);
                        }
                    }
                    else
                    {
                        DistanceTravelled += (Speed * (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed);
                    }
                }

                base.Update(gameTime);
            }
        }

        private void UpdateDoT(GameTime gameTime) 
        {
            float deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds * Session.singleton.Speed;

            DoTTimer += deltaSeconds;
            deltaHealth += dotHealth * deltaSeconds;

            int damage = (int)deltaHealth;

            if (damage > 0)
            {
                Health -= damage;
                deltaHealth = deltaHealth - damage;
            }

            if (Health <= 0)
            {
                Wave.Remove(this);
                Die();
                Session.singleton.AddMoney(IsBoss ? (int)(Wave.MoneyPerKill * Wave.BossMoneyScalar) : Wave.MoneyPerKill);

                if (DieEvent != null)
                    DieEvent(this, EventArgs.Empty);
            }
            else
            {
                hitTimer = 0.2f;
                hitDisplay = new Text(damage.ToString(), new Vector2(Rectangle.Right + 3, Rectangle.Top), new Vector2(Velocity.X, -Speed));
            }
        }

        internal void ApplyDoT(int damage, float time) 
        {
            if (dotHealth == 0 && DoTTime == 0)
            {
                deltaHealth = 0;
                dotHealth = damage;
                DoTTime = time;
                DoTTimer = 0;
            }
        }

        public void NewNodeInPath(Tile t) 
        {
            PathIndex++;
            Point TilePosition = Wave.Map.ToWorldCoordinates(t);
            Vector2 newVelocity = new Vector2(TilePosition.X - Position.X, TilePosition.Y - Position.Y);
            DistanceToTravel = newVelocity.Length();
            DistanceTravelled = 0;
            newVelocity.Normalize();
            Velocity = Vector2.Multiply(newVelocity, Speed);
            Rotation = (float)Math.Atan2(Velocity.Y, Velocity.X);
        }

        public void AddToWave(Wave w) 
        {
            Wave = w;
        }

        public void Hit(Bullet bullet, Tower owner) 
        {
            AudioManager.singleton.PlaySound(HitCueName);
            Health -= owner.CurrentStatistics.Damage;

            if (Health <= 0)
            {
                Wave.Remove(this);
                Die();
                Session.singleton.AddMoney(IsBoss ? (int)(Wave.MoneyPerKill * Wave.BossMoneyScalar) : Wave.MoneyPerKill);
                if (DieEvent != null)
                    DieEvent(this, EventArgs.Empty);
            }
            else
            {
                hitTimer = 0.4f;
                hitDisplay = new Text(
                    owner.CurrentStatistics.Damage.ToString(),
                    new Vector2(Rectangle.Right + 3, Rectangle.Top),
                    new Vector2(Velocity.X, -Speed));
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Delay <= 0)
            {
                Texture2D t = Texture;
                if (hitTimer > 0) Texture = HitTexture;

                base.Draw(gameTime, spriteBatch);

                if (hitTimer > 0 && hitDisplay != null) hitDisplay.Draw(spriteBatch, Session.singleton.UI.Font, Session.singleton.Map.ForeColour);
                Texture = t;
            }
        }

        #endregion

        #region Monster Reader

        public class MonsterReader : ContentTypeReader<Monster>
        {
            protected override Monster Read(ContentReader input, Monster existingInstance)
            {
                Monster result = new Monster();

                input.ReadRawObject<GameplayObject>(result as GameplayObject);
                result.Health = input.ReadInt32();
                result.MaxHealth = result.Health;
                result.TextureAsset = input.ReadString();
                result.Texture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Monsters\\{0}", result.TextureAsset));
                result.HitTextureAsset = input.ReadString();
                result.HitTexture = input.ContentManager.Load<Texture2D>(String.Format("Textures\\Monsters\\{0}", result.HitTextureAsset));
                result.HitCueName = input.ReadString();
                result.IsBoss = input.ReadBoolean();
                result.PathIndex = 0;

                return result;
            }
        }

        #endregion

        public Monster Clone()
        {
            Monster m = new Monster();

            m.Name = Name;
            m.Description = Description;
            m.Alpha = Alpha;
            m.Speed = Speed;
            m.Health = Health;
            m.MaxHealth = MaxHealth;
            m.TextureAsset = TextureAsset;
            m.Texture = Texture;
            m.HitTextureAsset = HitTextureAsset;
            m.HitCueName = HitCueName;
            m.HitTexture = HitTexture;
            m.IsBoss = IsBoss;
            m.PathIndex = -1;

            return m;
        }  
    }
}
