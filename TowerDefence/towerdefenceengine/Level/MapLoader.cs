﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace TowerDefenceEngine
{
    public class MapLoader
    {
        #region Writer

        [ContentTypeWriter]
        public class MapLoaderWriter : ContentTypeWriter<MapLoader>
        {
            protected override void Write(ContentWriter output, MapLoader value)
            {
                output.WriteObject(value.MapContentNames);
            }

            public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
            {
                return typeof(MapLoader.MapLoaderReader).AssemblyQualifiedName;
            }
        }

        #endregion

        #region Properties and Fields

        [ContentSerializer]
        private List<string> MapContentNames
        {
            get;
            set;
        }

        [ContentSerializerIgnore]
        public List<Map> Maps
        {
            get;
            private set;
        }

        [ContentSerializerIgnore]
        public static MapLoader singleton;

        #endregion

        #region Constructor

        public MapLoader()
        {
            MapContentNames = new List<string>();
            Maps = new List<Map>();
        }

        #endregion

        #region Reader

        public class MapLoaderReader : ContentTypeReader<MapLoader>
        {

            protected override MapLoader Read(ContentReader input, MapLoader existingInstance)
            {
                MapLoader mapLoader = new MapLoader();
                mapLoader.MapContentNames.AddRange(input.ReadObject<List<string>>());

                foreach (string s in mapLoader.MapContentNames)
                {
                    mapLoader.Maps.Add(input.ContentManager.Load<Map>(String.Format("Maps\\{0}\\{0}", s)));
                }

                return mapLoader;
            }
        }

        #endregion
    }
}
