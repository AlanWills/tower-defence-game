﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TowerDefenceEngine
{
    public class Image
    {
        #region Properties and Fields

        public Texture2D Texture
        {
            get;
            private set;
        }

        public Vector2 Position
        {
            get;
            private set;
        }

        public Rectangle Rectangle
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        public Image(Texture2D tex, Vector2 pos)
        {
            Texture = tex;
            Position = pos;
            Rectangle = new Rectangle((int)pos.X, (int)pos.Y, tex.Width, tex.Height);
        }

        #endregion

        #region Methods

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Rectangle, Color.White);
        }

        #endregion
    }
}
